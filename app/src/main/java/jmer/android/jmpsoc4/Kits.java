package jmer.android.jmpsoc4;

import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;

/**
 * Created by Jean on 2016-08-01.
 */

public final class Kits {

    //Region PSoC4_049
    private static final String[] PS0C4_049_pinNames = {"P0.0", "P0.1", "P0.2", "P0.3", "P0.4", "P0.5", "P0.6", "P1.0", "P1.1",
            "P1.2", "P1.3", "P1.4", "P1.5", "P1.6/LED1", "P2.0", "P2.1", "P2.2",
            "P2.3", "P2.4", "P2.5", "P2.6", "P2.7", "P3.0", "P3.1", "P3.4",
            "P3.5", "P3.6", "P3.7"};
    private static final String[] PS0C4_049_pinNum = {"0", "1", "2", "3", "4", "5", "6", "100", "101",
            "102", "103", "104", "105", "106", "200", "201", "202",
            "203", "204", "205", "206", "207", "300", "301", "304",
            "305", "306", "307"};

    private static final String[] PS0C4_049_pinDrive = {"HiZ analog","HiZ digital","Resistive PullUp","Resistive PullDown",
            "OpenDrainDrivesLow","OpenDrainDrivesHigh","Strong Drive 4/8 ma",
            "ResistivePullUpAndDown"    };

    private static final String[] processIDs ={"0", "1", "2", "3", "4", "5", "6", "7"};

    public String[] getPinNames_PS0C4_049_(){
        return PS0C4_049_pinNames;
    }

    public String[] getPinNum_PS0C4_049_(){
        return PS0C4_049_pinNum;
    }

    public String[] getPinDrive_PS0C4_049_(){
        return PS0C4_049_pinDrive;
    }

    public String[] getProcessIDs(){
        return processIDs;
    }


    //endregion PSoC4_049

}
