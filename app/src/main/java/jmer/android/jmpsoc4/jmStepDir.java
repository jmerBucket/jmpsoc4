package jmer.android.jmpsoc4;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Spinner;

import jmer.android.jmwpsoc4.R;

// GUI pour le module jmPSoC4_StepDir
public class jmStepDir extends Fragment {

    private  View view;
    private ArrayAdapter<String> aa;
    private String[] pinNames;
    private String[] pinNumbers;
    private String[] pinDrives;
    private String[] processIDs;
    private int pinStepSelected;
    private int driveModeSelected=6;
    private int processIdSelected;
    private int pinDirSelected;
    private String[] stepperIDs ={"0","1","2","3"};

    // interface de communication
    private jmStepDirListener mListener;
    public interface jmStepDirListener {
        void OnjmStepDirMessage(String msg);
    }

    public jmStepDir() {
        // Required empty public constructor
        Kits kit = new Kits();
        pinNames = kit.getPinNames_PS0C4_049_();
        pinNumbers = kit.getPinNum_PS0C4_049_();
        pinDrives = kit.getPinDrive_PS0C4_049_();
        processIDs = kit.getProcessIDs();
    }

    // vérification si l'interface de communication est installée dans
    // Lactivité principale
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof jmStepDirListener) {
            mListener = (jmStepDirListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_jm_stepdir, container, false);
        } catch (InflateException e) {
                /* map is already there, just return view as it is */
        }


        final CheckBox cbInv = (CheckBox)view.findViewById(R.id.cbInv) ;
        final EditText edDelay = (EditText)view.findViewById(R.id.edDelay) ;
        final EditText edCycles = (EditText)view.findViewById(R.id.edCycles) ;

        //region Spinners
        Spinner spinIDs = (Spinner)view.findViewById(R.id.spinnerId) ;
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, stepperIDs);

        spinIDs.setAdapter(adapter3);
        spinIDs.setSelection(0);
        processIdSelected = spinIDs.getSelectedItemPosition();

        spinIDs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                processIdSelected = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner spinStep = (Spinner)  view.findViewById(R.id.spinStep);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, pinNames);

        spinStep.setAdapter(adapter);
        spinStep.setSelection(15);
        pinStepSelected = spinStep.getSelectedItemPosition();

        spinStep.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pinStepSelected=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner spinDir = (Spinner)  view.findViewById(R.id.spinDir);

        ArrayAdapter<String> adapter4 = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, pinNames);

        spinDir.setAdapter(adapter4);
        spinDir.setSelection(14);
        pinDirSelected = spinDir.getSelectedItemPosition();

        spinDir.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pinDirSelected=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //endregion Spinners

        //region Boutons
        ImageButton ibBackward = (ImageButton)view.findViewById(R.id.ibBackward) ;
        ibBackward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg, inv;

                if(cbInv.isChecked())inv=" 0 ";
                else inv=" 1 ";
                // stepDir Id, stepPin, dirPin, dir, delay, cycles
                msg = "stepDir "+ stepperIDs[processIdSelected] +" "+
                        pinNumbers[pinStepSelected] +" " +
                        pinNumbers[pinDirSelected] + inv +
                        edDelay.getText() +" " +
                        edCycles.getText();
                mListener.OnjmStepDirMessage(msg);
            }
        });

        ImageButton ibFoward = (ImageButton)view.findViewById(R.id.ibFoward) ;
        ibFoward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg, inv;

                if(cbInv.isChecked())inv=" 1 ";
                else inv=" 0 ";
                // stepDir Id, stepPin, dirPin, dir, delay, cycles
                msg = "stepDir "+ stepperIDs[processIdSelected] +" "+
                        pinNumbers[pinStepSelected] +" " +
                        pinNumbers[pinDirSelected] + inv +
                        edDelay.getText() +" " +
                        edCycles.getText();
                mListener.OnjmStepDirMessage(msg);
            }
        });

        ImageButton ibPause = (ImageButton)view.findViewById(R.id.ibPause) ;
        ibPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg;
                int value=1;
                value = value << processIdSelected;

                // stepDirStop (value = id OR'ed)
                msg = "stepDirStop "+ value;

                mListener.OnjmStepDirMessage(msg);
            }
        });

        Button setDriveMode = (Button)view.findViewById(R.id.btSetDriveMode) ;

        // par défaut, la broche est configurée en strong drive 4/8ma
        setDriveMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = "jmBitMode "+pinNumbers[pinStepSelected] + " " +
                        driveModeSelected;
                mListener.OnjmStepDirMessage(msg);
            }
        });

        Button setDriveMode2 = (Button)view.findViewById(R.id.btSetDriveMode2) ;
        // par défaut, la broche est configurée en strong drive 4/8ma
        setDriveMode2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = "jmBitMode "+pinNumbers[pinDirSelected] + " " +
                        driveModeSelected;
                mListener.OnjmStepDirMessage(msg);
            }
        });
        //endregion Boutons

        //Region seekbar
        SeekBar sbStep =(SeekBar)view.findViewById(R.id.sbStep);
        sbStep.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String msg;
                int id = 1 << processIdSelected;
                int value = 255 -progress;
                if(value == 0)value=1;

                msg = "stepDirSpeed "+ id+ " " +value;
                mListener.OnjmStepDirMessage(msg);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        //Endregion Seekbar

        return view;
    }

    @Override
    public void onViewCreated(View jm_bit, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(jm_bit, savedInstanceState);


    }
}
