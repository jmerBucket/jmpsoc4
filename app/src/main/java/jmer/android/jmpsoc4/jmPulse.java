package jmer.android.jmpsoc4;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;

import jmer.android.jmwpsoc4.R;


public class jmPulse extends Fragment {

    private  View view;
    private ArrayAdapter<String> aa;
    private String[] pinNames;
    private String[] pinNumbers;
    private String[] pinDrives;
    private String[] processIDs;
    private int pinSelected;
    private int driveModeSelected;
    private int processIdSelected;

    // interface de communication
    private jmPulseListener mListener;
    public interface jmPulseListener {
        void OnjmPulseMessage(String msg);
    }

    public jmPulse() {
        // Required empty public constructor
        Kits kit = new Kits();
        pinNames = kit.getPinNames_PS0C4_049_();
        pinNumbers = kit.getPinNum_PS0C4_049_();
        pinDrives = kit.getPinDrive_PS0C4_049_();
        processIDs = kit.getProcessIDs();
    }

    // vérification si l'interface de communication est installée dans
    // Lactivité principale
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof jmPulseListener) {
            mListener = (jmPulseListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_jm_pulse, container, false);
        } catch (InflateException e) {
                /* map is already there, just return view as it is */
        }


        final CheckBox cbInv = (CheckBox)view.findViewById(R.id.cbInv) ;
        final EditText edTon = (EditText)view.findViewById(R.id.edTon) ;
        final EditText edToff = (EditText)view.findViewById(R.id.edToff) ;
        final EditText edCycles = (EditText)view.findViewById(R.id.edCycles) ;

        //region Spinners
        Spinner spinIDs = (Spinner)view.findViewById(R.id.spinnerId) ;
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, processIDs);

        spinIDs.setAdapter(adapter3);
        spinIDs.setSelection(0);
        processIdSelected = spinIDs.getSelectedItemPosition();

        spinIDs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                processIdSelected = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner pins = (Spinner)  view.findViewById(R.id.spinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, pinNames);

        pins.setAdapter(adapter);
        pins.setSelection(13);
        pinSelected = pins.getSelectedItemPosition();

        pins.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pinSelected=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final Spinner spinDrive = (Spinner) view.findViewById(R.id.spinner2);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, pinDrives);

        spinDrive.setAdapter(adapter2);
        spinDrive.setSelection(6);
        driveModeSelected = spinDrive.getSelectedItemPosition();

        spinDrive.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                driveModeSelected = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //endregion Spinners

        //region Boutons
        Button btHigh = (Button)view.findViewById(R.id.btHigh);

        btHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg;
                if(cbInv.isChecked()) msg = "pulse "+ processIDs[processIdSelected] +" "
+                        pinNumbers[pinSelected] +" 1 0 ";
                else msg = "pulse "+ processIDs[processIdSelected] +" "+
                        pinNumbers[pinSelected] +" 0 1 ";

                mListener.OnjmPulseMessage(msg);
            }
        });

        Button btLow = (Button)view.findViewById(R.id.btLow);

        btLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg;
                if(cbInv.isChecked()) msg = "pulse "+ processIDs[processIdSelected] +" "+
                        pinNumbers[pinSelected] +" 0 1 ";
                else msg = "pulse "+ processIDs[processIdSelected] +" "+
                        pinNumbers[pinSelected] +" 1 0 ";
                mListener.OnjmPulseMessage(msg);
            }
        });

        Button btToggle = (Button)view.findViewById(R.id.btToggle);

        btToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "pulse "+ processIDs[processIdSelected]+ " " +
                        pinNumbers[pinSelected] +" 0 0";
                mListener.OnjmPulseMessage(msg);
            }
        });

        Button btPulse = (Button)view.findViewById(R.id.btPulse);

        btPulse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String on, off;
                if(cbInv.isChecked()){
                    off = edTon.getText().toString();
                    on = edToff.getText().toString();
                }
                else{
                    off = edToff.getText().toString();
                    on = edTon.getText().toString();
                }
                String msg = "pulse "+ processIDs[processIdSelected]+ " " +pinNumbers[pinSelected] +
                        " " +on +" " +off + " " +edCycles.getText();
                mListener.OnjmPulseMessage(msg);
            }
        });

        // Par défaut le module jmPSoC4_GPIO est programmé
        //
        Button btSet = (Button)view.findViewById(R.id.btSetDriveMode);
        btSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "jmBitMode "+pinNumbers[pinSelected] + " " +
                        driveModeSelected;
                mListener.OnjmPulseMessage(msg);
            }
        });
        //endregion Boutons

        //Region seekbar
        SeekBar sbPulse =(SeekBar)view.findViewById(R.id.sbPulse);
        sbPulse.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int on, off;
                if(cbInv.isChecked()){
                    off = progress;
                    on = 20 - progress;
                }
                else{
                    off = 20-progress;
                    on = progress;
                }
                String msg = "pulse "+ processIDs[processIdSelected]+ " " +pinNumbers[pinSelected] +
                        " " +on +" " +off + " " +edCycles.getText();
                mListener.OnjmPulseMessage(msg);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        //Endregion Seekbar

        return view;
    }

    @Override
    public void onViewCreated(View jm_bit, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(jm_bit, savedInstanceState);


    }
}
