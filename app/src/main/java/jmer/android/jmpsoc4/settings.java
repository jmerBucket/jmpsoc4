package jmer.android.jmpsoc4;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.content.SharedPreferences;

import jmer.android.jmwpsoc4.R;


public class settings extends AppCompatActivity {

    SharedPreferences prefs;
    String prefName = "jmWiFiSettings";

    // Return Intent extra
    public String WiFiIP ;
    public int WiFiPort;
    public int port;

    private EditText edServerIP, edServerPort;
    Button btOK, btCancel;

    //wifi and network states
    ConnectivityManager connectivity;
    NetworkInfo wifiNetworkInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // last Server IP Port used
        prefs = getSharedPreferences(prefName, MODE_PRIVATE);

        connectivity = (ConnectivityManager)  getSystemService(Context.CONNECTIVITY_SERVICE);
        wifiNetworkInfo = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        // activer Wifi
        if (!wifiNetworkInfo.isConnected()) wifiManager.setWifiEnabled(true);

        edServerIP=(EditText)findViewById(R.id.edServerIP);
        edServerPort=(EditText)findViewById(R.id.edServerPort);
        btOK = (Button)findViewById(R.id.btOK);
        btCancel = (Button)findViewById(R.id.btCancel);

        // last user WiFiBot server IP,Port
        String prefIP = prefs.getString("IP", "192.168.4.1");
        edServerIP.setText(prefIP);
        port = prefs.getInt("PORT", 12000);
        edServerPort.setText(Integer.toString(port));

        btOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ip = edServerIP.getText().toString();
                if(edServerPort.getText()!=null)
                port = Integer.parseInt(edServerPort.getText().toString());

                // save user data key, value
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("IP", ip);
                editor.putInt("PORT", port);
                editor.commit();

                // valeurs transmises au MainActivity
                Intent intent = new Intent();
                intent.putExtra("WiFiIP",ip);
                intent.putExtra("WiFiPort",port);
                setResult(RESULT_OK,intent);
                finish();

            }
        });

        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_CANCELED,intent);
                finish();
            }
        });

    }

}
