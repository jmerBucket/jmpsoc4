package jmer.android.jmpsoc4;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;

import jmer.android.jmwpsoc4.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link } interface
 * to handle interaction events.
 * Use the {@link jmBit#newInstance} factory method to
 * create an instance of this fragment.
 */
public class jmBit extends Fragment {

    private  View view;
    private ArrayAdapter<String> aa;
    private String[] pinNames;
    private String[] pinNumbers;
    private String[] pinDrives;
    private int pinSelected;
    private int driveModeSelected;

    // interface de communication
    private jmBitListener mListener;
    public interface jmBitListener {
        void OnjmBitMessage(String msg);
    }


    // vérification si l'interface de communication est installée dans
    // Lactivité principale
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof jmBitListener) {
            mListener = (jmBitListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public jmBit() {
        Kits kit = new Kits();
        pinNames = kit.getPinNames_PS0C4_049_();
        pinNumbers = kit.getPinNum_PS0C4_049_();
        pinDrives = kit.getPinDrive_PS0C4_049_();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment BlankFragment.
     */
    public static jmBit newInstance() {
        jmBit fragment = new jmBit();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_jm_bit, container, false);

        final CheckBox cbInv = (CheckBox)view.findViewById(R.id.cbInv) ;

        //Region Spinner
        Spinner pins = (Spinner)  view.findViewById(R.id.spinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, pinNames);

        pins.setAdapter(adapter);
        pins.setSelection(13);
        pinSelected = pins.getSelectedItemPosition();

        pins.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pinSelected=position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final Spinner spinDrive = (Spinner) view.findViewById(R.id.spinner2);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_dropdown_item, pinDrives);

        spinDrive.setAdapter(adapter2);
        spinDrive.setSelection(6);
        driveModeSelected = spinDrive.getSelectedItemPosition();

        spinDrive.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                driveModeSelected = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //endregion Spinners

        //Region Boutons
        Button btHigh = (Button)view.findViewById(R.id.btHigh);

        btHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg;
                if(cbInv.isChecked()) msg = "jmBit "+ pinNumbers[pinSelected] +" 0";
                else msg = "jmBit "+ pinNumbers[pinSelected] +" 1";
                mListener.OnjmBitMessage(msg);
            }
        });

        Button btLow = (Button)view.findViewById(R.id.btLow);

        btLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg;
                if(cbInv.isChecked()) msg = "jmBit "+ pinNumbers[pinSelected] +" 1";
                else msg = "jmBit "+ pinNumbers[pinSelected] +" 0";
                mListener.OnjmBitMessage(msg);
            }
        });

        Button btToggle = (Button)view.findViewById(R.id.btToggle);

        btToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "jmBit "+ pinNumbers[pinSelected] +" 2";
                mListener.OnjmBitMessage(msg);
            }
        });

        Button btSet = (Button)view.findViewById(R.id.btSetDriveMode);
        btSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "jmBitMode "+pinNumbers[pinSelected] + " " +
                        driveModeSelected;
                mListener.OnjmBitMessage(msg);
            }
        });
        //EndRegion Boutons

        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}
