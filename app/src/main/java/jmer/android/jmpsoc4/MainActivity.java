package jmer.android.jmpsoc4;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import android.support.v7.widget.Toolbar;
import android.app.AlertDialog;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

import jmer.android.jmwpsoc4.R;

import static jmer.android.jmwpsoc4.R.id.fragmentUI;


public class MainActivity extends AppCompatActivity implements
        BlankFragment.OnFragmentInteractionListener,
        jmBit.jmBitListener,
        jmStepDir.jmStepDirListener,
        jmPulse.jmPulseListener
        {
    //region Variables globales
    static String SERVER_IP;
    static int PORT = 1234;

    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

    // Codes de l'Intent
    private static final int REQUEST_SETTINGS = 1;

    Handler handler = new Handler();
    static Socket socket;
    PrintWriter printWriter;

    ScrollView msgScroll;
    TextView tvMsg;
    Button btClear;
    Toolbar myToolbar;

    private final int off = 0;
    int longueur = 0;
    //endregion

    // region On Create
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remplace le fragmentUI définit dans activity_main.xml
        // par le fragment désiré
        if (savedInstanceState == null) {
            // Let's first dynamically add a fragment into a frame container
            getSupportFragmentManager().beginTransaction().
                    add(fragmentUI, new jmBit(), "UI").
                    commit();
        }

        setContentView(R.layout.activity_main);
        tvMsg = (TextView) findViewById(R.id.tvMsg);
        msgScroll=(ScrollView)findViewById(R.id.msgScroll) ;
        btClear = (Button) findViewById(R.id. btClear);
        btClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvMsg.setText("");
                message("Messages:\n");
            }
        });

        //region Toolbar
        myToolbar = (Toolbar) findViewById(R.id.app_bar);
        if (myToolbar != null) {
            setSupportActionBar(myToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            //getSupportActionBar().setDisplayShowHomeEnabled(true);
            //getSupportActionBar().setDisplayUseLogoEnabled(true);

        }
        //myToolbar.setTitle("PSoC4");

        //endregion toolbar
    }//endregion

    @Override
    public void onFragmentInteraction(String msg) {
        message(msg);
       // TxMsg(msg);
    }

    @Override
    public void OnjmStepDirMessage(String msg) {
        message(msg);
        TxMsg(msg);
    }

    @Override
    public void OnjmBitMessage(String msg) {
       // Toast toast = Toast.makeText(this, "Wheeee!",Toast.LENGTH_SHORT);
       // toast.show();
        message(msg);
        TxMsg(msg);
    }

    @Override
    public void OnjmPulseMessage(String msg) {
        message(msg);
        TxMsg(msg);
    }

    //region Client Thread
    public class ClientThread implements Runnable {
        public void run() {
            try {
                InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                       message("Connecting to WiFi");
                    }
                });

                socket = new Socket(serverAddr, PORT);

                try {
                    printWriter = new PrintWriter(new BufferedWriter(
                            new OutputStreamWriter(socket.getOutputStream())),
                            true);

                    //---get an InputStream object to read from the server---
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(socket.getInputStream()));
                    try {
                        //---read all incoming data terminated with a \n
                        // char---
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            final String strReceived = line;
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                   message(strReceived);
                                }
                            });
                        }

                        //---disconnected from the server---
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                               message("WiFi disconnected");
                            }
                        });
                    } catch (Exception e) {
                        final String error = e.getLocalizedMessage();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                               message(error);
                            }
                        });
                    }

                } catch (Exception e) {
                    final String error = e.getLocalizedMessage();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                           message(error);
                        }
                    });
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                       message("Connection closed.");
                    }
                });
            } catch (Exception e) {
                final String error = e.getLocalizedMessage();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                       message(error);
                    }
                });
            }
        }
    }
    //endregion ClientThread

    //region Messages scrollview
    private void message(String s) {
        tvMsg.append(s+"\n");

        	msgScroll.post(new Runnable(){
        	 public void run(){
        		 msgScroll.fullScroll(View.FOCUS_DOWN);
        	 }
        });
    }
    //endregion

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {
            socket.shutdownInput();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //region Menu
    /*
    * Methodes de gestion du menu
    * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    // Constructeur
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }


    // Methodes de traitement du choix dans le menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String mode = null;

        switch (item.getItemId()) {

            case R.id.jmBit:
               message("jmBit");
                // instance du fragement a afficher

                // dynamically add a fragment into a frame container
                getSupportFragmentManager().beginTransaction().
                        replace(fragmentUI, new jmBit(), "jmBit").
                        commit();

                return (true);

            case R.id.jmPulse:
                message("jmPulse");

                // dynamically add a fragment into a frame container
                getSupportFragmentManager().beginTransaction().
                        replace(fragmentUI, new jmPulse(), "jmPulse").
                        commit();
                return (true);

            case R.id.jmStepper:
                message("jmStepDir");
                // dynamically add a fragment into a frame container
                getSupportFragmentManager().beginTransaction().
                        replace(fragmentUI, new jmStepDir(), "jmStepDir").
                        commit();
                return (true);


            case R.id.jmMotor:
                message("jmMotor");
                // dynamically add a fragment into a frame container
                getSupportFragmentManager().beginTransaction().
                        replace(fragmentUI, new BlankFragment(), "blank2").
                        commit();
                return (true);


            case R.id.about:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("jmPSoC4 1.0\nAsTuVuComment@gmail.com");
                builder.setMessage("Contrôle du PSoC4 kit049 par WiFi").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // do things if you want to
                            }
                        }
                );
                AlertDialog alert = builder.create();
                alert.show();
               message("À Propos");
                return true;

            case R.id.settings:
                serverID();
                return true;

            case R.id.quit: // Choix : "Quitter"
                finish();     // Fermeture de l'application
                return true;
        }

        return false;
    }

    protected void serverID() {
        // Sélectionner le serveur
        Intent serverIntent = new Intent(this, settings.class);
        // Demarrer l'activité "activity_settings" avec l'identifiant `
        // de reponse REQUEST_CONNECT_DEVICE
        startActivityForResult(serverIntent, REQUEST_SETTINGS);
    }

    //region Transmit message
    private void TxMsg(String mode) {
        // transmet un message TCP
        if (mode != null) {
            message(mode);
            final String msg = mode;
            if(socket != null) {
                if (socket.isConnected()) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            printWriter.println(msg);
                        }
                    });
                }
            }
            else message("Pas de Connexion");
        }

    }//endregion Transmit Mess
    //endregion

    // Call back a la fermeture d'une activity
    // Identifie la reponse pour realiser le traitement correspondant
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_SETTINGS:
                //
                if (resultCode == Activity.RESULT_OK) // "activity_settings" se termine normalement
                {// Connexion au serveur choisie
                    // Lecture des parametres du serveur ESP8266 sur WiFiBot
                    SERVER_IP = data.getStringExtra("WiFiIP");
                    PORT = data.getIntExtra("WiFiPort",12345);

                    message("Serveur: "+SERVER_IP+": "+PORT);
                    // Connexion au serveur
                    Thread clientThread = new Thread(new ClientThread());
                    clientThread.start();
                }
                break;
        }
    }
    //endregion

}
